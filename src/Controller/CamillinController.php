<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CamillinController extends AbstractController
{
    /**
     * @Route("/camillin", name="camillin")
     */
    public function index()
    {
        return $this->render('camillin/index.html.twig', [
            'controller_name' => 'CamillinController',
        ]);
    }


}

