<?php

namespace App\Controller;

use App\Entity\Definition;

use App\Form\DefinitionType;

use App\Form\SearchNomType;
use App\Repository\DefinitionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
//use FOS\ElasticaBundle\Elastica;


/**
 * @Route("/definition")
 */
class DefinitionController extends AbstractController
{

    /**
     * @Route("/", name="definition_index", methods={"GET"})
     */
    public function index(DefinitionRepository $definitionRepository, Request $request): Response
    {
//        $newNom = new Definition();
//        $formNom=$this->createForm(SearchNomType::class,$newNom);
//        $formNom->handleRequest($request);
//        if ($formNom->isSubmitted()&& $formNom->isValid()){
//            $entityManager=$this->getDoctrine()->getManager();
//            $entityManager->persist($newNom);
//            $entityManager->flush();
//        }
        return $this->render('definition/index.html.twig', [
            'definitions' => $definitionRepository->findAll(),
//            'formNom'=>$formNom->createView(),
        ]);
    }


    /**
     * @Route("/new", name="definition_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $definition = new Definition();
        $form = $this->createForm(DefinitionType::class, $definition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($definition);
            $entityManager->flush();

            return $this->redirectToRoute('definition_index');
        }

        return $this->render('definition/new.html.twig', [
            'definition' => $definition,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="definition_show", methods={"GET"})
     */
    public function show(Definition $definition): Response
    {
        return $this->render('definition/show.html.twig', [
            'definition' => $definition,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="definition_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Definition $definition): Response
    {
        $form = $this->createForm(DefinitionType::class, $definition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('definition_index');
        }

        return $this->render('definition/edit.html.twig', [
            'definition' => $definition,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="definition_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Definition $definition): Response
    {
        if ($this->isCsrfTokenValid('delete'.$definition->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($definition);
            $entityManager->flush();
        }

        return $this->redirectToRoute('definition_index');
    }
    /**
     * @Route("/search", name="blog_search" , methods={"GET","POST"})

     */
    public function search(Request $request, DefinitionRepository $definitionRepository, $nom): Response
    {

        $array=$definitionRepository->findAll();
        foreach ($array as $definition){
       $nom=$definition->getNom();
        }
        $foundDefinitions = $nom->findBySearchNom($nom);

        $results = [];
        foreach ($foundDefinitions as $definition) {
            $results[] = [
                'nom' => $nom,

            ];
        }

        return $this->json($results);
    }



}

