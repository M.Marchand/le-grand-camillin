<?php

namespace App\Controller;

use App\Entity\Definition;
use App\Form\SearchNomType;
use App\Repository\DefinitionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\ElasticaBundle\Elastica;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search" ,  methods={"GET","POST"})
     */
//    public function index()
//    {
//        return $this->render('search/index.html.twig', [
//            'controller_name' => 'SearchController',
//        ]);
//    }
    public function index(DefinitionRepository $definitionRepository, Request $request): Response
    {
        $newNom = new Definition();
        $formNom = $this->createForm(SearchNomType::class, $newNom);
        $formNom->handleRequest($request);
        if ($formNom->isSubmitted() && $formNom->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newNom);
            $entityManager->flush();
        }
        $array = $definitionRepository->findAll();
        foreach ($array as $definition) {
            $nom = $definition->getNom();
        }
        $foundDefinitions = $nom;

        $results = [];
        foreach ($definition as $foundDefinitions) {
            $results[] = [
                'nom' => $nom,

            ];
        }

        return $this->render('search/index.html.twig', [
            'definitions' => $definitionRepository->findAll(),
            'formNom' => $formNom->createView(),
            $this->json($results),

        ]);
    }}



//
//$array=$definitionRepository->findAll();
//        foreach ($array as $definition){
//            $nom=$definition->getNom();
//        }
//        $foundDefinitions = $nom->findBySearchNom($nom);
//
//        $results = [];
//        foreach ($foundDefinitions as $definition) {
//            $results[] = [
//                'nom' => $nom,
//
//            ];
//        }
//
//
//
//        return $this->render('search/search_41.html.twig', compact('results', 'q'));
//    }

