<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\ElasticaBundle\Elastica;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DefinitionRepository")
 */
class Definition
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     */
    private $Definition;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Exemple;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->Nom;
    }

    public function setNom(string $Nom): self
    {
        $this->Nom = $Nom;

        return $this;
    }

    public function getDefinition(): ?string
    {
        return $this->Definition;
    }

    public function setDefinition(?string $Definition): self
    {
        $this->Definition = $Definition;

        return $this;
    }

    public function getExemple(): ?string
    {
        return $this->Exemple;
    }

    public function setExemple(?string $Exemple): self
    {
        $this->Exemple = $Exemple;

        return $this;
    }
}
